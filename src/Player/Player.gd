extends KinematicBody2D

# velocita' dell'omino
export var speed: float = 500.0
# velocita' dell'omino al salto
export var jump_speed: float = -650.0
# gravita' dell'omino
export var gravity: float = 1500.0

# referenze a animatre e sprite
onready var anim: AnimationPlayer = $AnimationPlayer
onready var sprite: Sprite = $Sprite

# state della state machine
enum { IDLE, RUN, JUMP, FALL }

# velocita' iniziale
var velocity: = Vector2.ZERO

# stato iniziale
var state: int = IDLE setget set_state

# animazione iniziale
var current_animation: String = "idle"

# Chiamata quando viene istanziata la scena
func _ready() -> void:
	# di default l'omino e' nello stato IDLE
	self.state = IDLE

# Chiamata ad ogni frame
func _physics_process(delta: float) -> void:
	# Controllo se ci sono arrivati degli input dal giocatore
	get_input(delta)	
	# muovo l'omino e aggiorno la suo velocita (metti che e' andato a sbattere dopo essersi mosso)
	velocity = move_and_slide(velocity, Vector2.UP)
	# controllo il suo nuovo stato (si e' fermato, cade, corre, salta)
	check_state()

func get_input(delta: float) -> void:
	# vediamo se devo andare a destra, sinistra o stare fermo
	var dir: = get_move_direction()
	
	# i kinematicbody sono privi di fisica, la gravita' si da a mano
	velocity.y += gravity * delta
	
	# aggiorno la sua velocita' orizzontale in base alla direzione
	velocity.x = dir.x * speed
	
	# se il giocatore preme su e l'omino e a terra saltiamo
	if Input.is_action_just_pressed("ui_up") and is_on_floor():
		velocity.y = jump_speed
	
func check_state() -> void:
	# controllo gli stati e muovo la state machine
	if state == RUN and velocity.x == 0:
		self.state = IDLE

	if state == IDLE and velocity.x != 0:
		self.state = RUN		
	
	if state != JUMP and velocity.y < 0:
		self.state = JUMP
	
	if state != FALL and velocity.y > 0:
		self.state = FALL
	
	if state in [JUMP, FALL] and velocity.y == 0:
		self.state = IDLE


func get_move_direction() -> Vector2:
	# trick per vedere se il tipo ha premuto destra, sinistra o tutti e due
	var dir = Vector2(
		Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left"),
		1.0
	)
	
	# se vado a destra giro la sprite verso il dritto (il suo default)
	if dir.x > 0:
		sprite.flip_h = false
	
	# se vado a sinistra ribalto la sprite
	if dir.x < 0:
		sprite.flip_h = true
			
	return dir

# setter del valore dello stato
func set_state(value: int) -> void:
	change_state(value)
	state = value

# in base a come cambia lo stato aggiorno l'animazione
func change_state(value):
	match value:
		IDLE:
			current_animation = "idle"
		RUN:
			current_animation = "run"
		JUMP:
			current_animation = "jump"
		FALL:
			current_animation = "fall"
	
	anim.play(current_animation)
	
